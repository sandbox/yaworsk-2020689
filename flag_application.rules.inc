<?php
/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function flag_application_rules_event_info() {
  return array(
    'flag_application_reviewed' => array(
      'group' => t('Flag application'),
      'label' => t('A flag application was reviewed'),
      'module' => 'flag_application',
      'variables' => array(
        'fcid' => array(
          'type' => 'text',
          'label' => t('Fcid'),
          'description' => t('The flag id of the application'),    
        ),
        'status' => array(
          'type' => 'text',
          'label' => t('status of the application'), 
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info()
 */
function flag_application_rules_condition_info() {
  return array(
    'flag_application_condition_review_status' => array(
      'group' => t('Flag application'),
      'label' => t('The application is of status'),
      'parameter' => array (
        'application_fcid' => array(
          'type' => 'text',
          'label' => t('The flag id of the application'),    
        ),
        'application_status' => array (
          'type' => 'text',
          'label' => t('Status of the application'),
          'restriction' => 'input',
          'options list' => '_flag_application_conditions_options_list',
        ),  
      ),
      'module' => 'flag_application',
    ),
  );
}

/**
 * Function required by hook_rules_condition_info to provide an options list
 * 
 * @return
 *   An array of options
 */
function _flag_application_conditions_options_list () {
  return array (
    '1' => 'Approved',
    '2' => 'Denied',
  );
}

/**
 * Callback for condition evaluation in hook_rules_condition_info
 * 
 * @return
 *   Boolean TRUE or FALSE depending on whether the condition is met
 */
function flag_application_condition_review_status($fcid, $status) {
  $fcid_status = db_query("SELECT status FROM {flag_application} WHERE fcid = :fcid", array(':fcid' => $fcid))->fetchField();
  return ($fcid_status == $status) ? TRUE : FALSE;
}